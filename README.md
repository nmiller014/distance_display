# Distance display tool
This is a demo tool to read distance from a Sparkfun HC-SR04 breakout board and display it on an Adafruit SSD1306 128x32 OLED breakout board. This should run on just about any flavor of Arduino.  

## Parts
* [HC-SR04](https://www.sparkfun.com/products/13959)
* [SSD1306 128x32 OLED](https://www.adafruit.com/product/661)
