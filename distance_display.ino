#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

const int HCSR04_TRIG = 12;
const int HCSR04_ECHO = 11;
const unsigned int HCSR04_MAX_DIST = 23200;

#define OLED_MOSI  4
#define OLED_CLK   5
#define OLED_DC    8
#define OLED_CS    10
#define OLED_RESET 9
Adafruit_SSD1306 display(OLED_MOSI, OLED_CLK, OLED_DC, OLED_RESET, OLED_CS);

#if (SSD1306_LCDHEIGHT != 32)
#error("Height incorrect, please fix Adafruit_SSD1306.h!");
#endif

int measure_distance(void);
void update_display(int cm);

void setup()   {                
  pinMode(HCSR04_TRIG, OUTPUT);
  digitalWrite(HCSR04_TRIG, LOW);
  
  // by default, we'll generate the high voltage from the 3.3v line internally! (neat!)
  display.begin(SSD1306_SWITCHCAPVCC);
}

void loop() {
  int cm = measure_distance();

  update_display(cm);
}

void update_display(int cm) {
  // Clear the buffer.
  display.clearDisplay();

  // Update the display
  display.setCursor(0,0);
  display.setTextSize(4);
  display.setTextColor(WHITE);
  display.print(cm);
  display.println("cm");
  display.display();
}

int measure_distance(void) {
  // This code is mostly ripped from the Sparkfun example (https://www.sparkfun.com/products/13959)
  // The only differences are:
  //    I removed the inch conversion
  //    I moved the delay to before so that my display was more in sync with my measurements since I update the display after measuring
  //    I changed the calculation to fixed point instead of floating point

  unsigned long t1;
  unsigned long t2;
  unsigned long pulse_width;

  // Wait at least 60ms before next measurement
  delay(60);

  // Hold the trigger pin high for at least 10 us
  digitalWrite(HCSR04_TRIG, HIGH);
  delayMicroseconds(10);
  digitalWrite(HCSR04_TRIG, LOW);

  // Wait for pulse on echo pin
  while ( digitalRead(HCSR04_ECHO) == 0 );

  // Measure how long the echo pin was held high (pulse width)
  // Note: the micros() counter will overflow after ~70 min
  t1 = micros();
  while ( digitalRead(HCSR04_ECHO) == 1);
  t2 = micros();
  pulse_width = t2 - t1;

  // Calculate distance in centimeters. The constants
  // are found in the datasheet, and calculated from the assumed speed
  //of sound in air at sea level (~340 m/s).
  return ((pulse_width*1000) / 58) / 1000;
}
